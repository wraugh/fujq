# fujq - Javascript-powered command-line JSON processor

[jq](https://stedolan.github.io/jq/) is great for simple filters, but more
complex queries are awkward to write. fujq lets you process JSON documents using
Javascript.

For example, given the following JSON document,

    [{"id": 1}, {"id": 2}, {"id": 3}]

I can't figure out how to instruct jq to do something like

> give me all objects except those with ids 1 or 2.

With fujq it's just javascript:

    $ echo '[{"id": 1}, {"id": 2}, {"id": 3}]' |\
    >     fujq 'let excl = [1, 2]; return d.filter(i => !excl.includes(i.id))'
    [{"id":3}]

## How it works

fujq parses stdin as a json document and stores the resulting object in variable
`d`. Then it evals your script, and outputs the returned value as JSON on
stdout.

## Installing

    npm install fujq
