#!/usr/bin/env node

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

if (process.argv.length < 3) {
    throw new Error("Missing script argument");
}
if (process.argv.length > 3) {
    throw new Error("Too many arguments; expecting a single script");
}
var script = process.argv[2];
eval(`var fn = (d) => { ${script} }`);
var doc = '';
var fujq = () => {
    j = JSON.parse(doc);
    process.stdout.write(JSON.stringify(fn(j)) + "\n");
};

// Read all of stdin into string doc, then pass it to the caller's script
process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
    const chunk = process.stdin.read();
    if (chunk !== null) {
        doc += chunk;
    }
});
process.stdin.on('end', fujq);
